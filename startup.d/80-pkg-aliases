if [ -n "${PKG_BACKEND}" ];
then
	PKG_BACKEND="${PKG_BACKEND}"
elif [ -r "/etc/redhat-release" ];
then
	case "$(head -n 1 /etc/redhat-release | cut -d' ' -f1)" in
		Fedora)
			PKG_BACKEND="dnf"
		;;
	esac
elif [ -f "/etc/openwrt_release" ];
then
	PKG_BACKEND="opkg"
elif [ "$(lsb_release --short --id 2>/dev/null)" = "openSUSE project" ];
then
	PKG_BACKEND="zypper"
elif builtin type aptitude >/dev/null 2>&1;
then
	PKG_BACKEND="aptitude"
elif builtin type apt-get >/dev/null 2>&1;
then
	PKG_BACKEND="apt-utils"
fi

completion_wrapper() {
	builtin local aliasname="$1"
	builtin local user_name="$2"
	builtin local argc=$(($#-3))
	builtin shift 2
	
	# Register alias
	if [ -n "${user_name}" ] && builtin type sudo >/dev/null 2>&1;
	then
		builtin alias "${aliasname}"="sudo -u \"${user_name}\" $*"
	elif [ -n "${user_name}" ] && builtin type su >/dev/null 2>&1;
	then
		builtin alias "${aliasname}"="su \"${user_name}\" -c \"$*\""
	else
		builtin alias "${aliasname}"="$*"
	fi
	
	# Create completion function for alias
	if [ "${PKG_COMPLETE}" = "true" ];
	then
		eval "
			function __COMP_${aliasname} {
				# Adjust current word selected
				((COMP_CWORD+=${argc}))
			
				# Save old input line
				builtin local line=\"\${COMP_LINE}\"
			
				# Adjust list of words available on commandline
				COMP_WORDS=( "$@" \${COMP_WORDS[@]:1} )
				COMP_LINE=\"\${COMP_WORDS[*]}\"
			
				# Adjust current cursor position on commandline
				COMP_POINT=\"\$((\${COMP_POINT}+\${#COMP_LINE}-\${#line}))\"
				builtin unset -v line
			
				builtin local COMP_CMD=''
				# Determine the actual completion command
				builtin set -- \$(complete -p \"${1}\")
				builtin local OPTOPT OPTARG OPTERR OPTIND=2
				while builtin getopts 'o:F:' OPTOPT;
				do
					if [ \"\${OPTOPT}\" = 'F' ];
					then
						COMP_CMD=\"\${OPTARG}\"
					fi
				done
				builtin unset OPTOPT OPTIND OPTARG OPTERR
			
				# Call the actual completer
				\"\${COMP_CMD}\"
			
				return 0
			}
		"

		# Register completion function for alias
		builtin complete -o default -F "__COMP_${aliasname}" "${aliasname}"
	fi
}


completion_include()
{
	if [ -r "/usr/share/bash-completion/bash_completion" ];
	then
		builtin source "/usr/share/bash-completion/bash_completion"
		
		while [ $# -gt 0 ];
		do
			builtin source "/usr/share/bash-completion/completions/${1}"
			
			builtin shift 1
		done
		
		PKG_COMPLETE=true
	elif [ -r "/etc/bash_completion" ];
	then
		builtin source "/etc/bash_completion"
		
		PKG_COMPLETE=true
	elif [ -r "/etc/bash_completion.d" ];
	then
		while [ $# -gt 0 ];
		do
			builtin source "/etc/bash_completion.d/${1}.sh"
			
			builtin shift 1
		done
		
		PKG_COMPLETE=true
	else
		PKG_COMPLETE=false
	fi
}


if [ "${PKG_BACKEND}" = "dnf" ];
then
	completion_include dnf
	
	# dnf
	# Note: Parameters like `--cacheonly` need to be at the end of the commandline to not
	#       not disturb completion.
	completion_wrapper "pkg-autoremove"  "root" "dnf" "autoremove"  "--cacheonly"
	completion_wrapper "pkg-build-dep"   "root" "dnf" "builddep"
	completion_wrapper "pkg-clean"       "root" "dnf" "clean"       "--cacheonly"
	completion_wrapper "pkg-download"    ""     "dnf" "download"
	#completion_wrapper "pkg-forget-new"  "root" "dnf" "forget-new"  "--cacheonly"
	completion_wrapper "pkg-hold"        "root" "dnf" "versionlock" "add"    "--cacheonly"
	completion_wrapper "pkg-unhold"      "root" "dnf" "versionlock" "delete" "--cacheonly"
	completion_wrapper "pkg-info"        ""     "dnf" "info"        "--cacheonly"
	completion_wrapper "pkg-install"     "root" "dnf" "install"
	completion_wrapper "pkg-mark-auto"   "root" "dnf" "mark" "remove"  "--cacheonly"
	completion_wrapper "pkg-mark-manual" "root" "dnf" "mark" "install" "--cacheonly"
	completion_wrapper "pkg-purge"       "root" "dnf" "remove"      "--cacheonly"
	completion_wrapper "pkg-reinstall"   "root" "dnf" "reinstall"
	completion_wrapper "pkg-remove"      "root" "dnf" "remove"      "--cacheonly"
	completion_wrapper "pkg-search"      ""     "dnf" "search"      "--cacheonly"
	completion_wrapper "pkg-source"      ""     "dnf" "download"    "--source"
	
	completion_wrapper "pkg-depends"  "" "dnf" "repoquery" "--cacheonly" "--requires"
	completion_wrapper "pkg-files"    "" "dnf" "repoquery" "--cacheonly" "--list"
	completion_wrapper "pkg-owns"     "" "dnf" "repoquery" "--cacheonly" "--file"
	completion_wrapper "pkg-provides" "" "dnf" "repoquery" "--cacheonly" "--provides"
	completion_wrapper "pkg-rdepends" "" "dnf" "repoquery" "--cacheonly" "--whatrequires"
	completion_wrapper "pkg-versions" "" "dnf" "repoquery" "--cacheonly" "--nvr"
	
	completion_wrapper "pkg-refresh" "root" "dnf" "check-update" "--refresh" "--debuglevel" "3"
	completion_wrapper "pkg-update"  "root" "dnf" "upgrade"
	completion_wrapper "pkg-upgrade" "root" "dnf" "upgrade"
	
	# rpm:
	completion_wrapper "pkg-changelog" ""     "rpm" "--query" "--changelog"
	#completion_wrapper "pkg-configure" "root"
	
elif [ "${PKG_BACKEND}" = "opkg" ];
then
	__opkg_search() {
		opkg find "*${1}*"
	}
	
	__opkg_update() {
		builtin local packages="$(opkg list-upgradable | cut -d' ' -f1)"
		opkg upgrade ${packages}
	}
	
	completion_wrapper "pkg-configure"      "root" "opkg" "configure"
	completion_wrapper "pkg-download"       "root" "opkg" "download"
	completion_wrapper "pkg-files"          ""     "opkg" "files"
	completion_wrapper "pkg-hold"           "root" "opkg" "flag" "hold"
	completion_wrapper "pkg-unhold"         "root" "opkg" "flag" "ok"
	completion_wrapper "pkg-info"           ""     "opkg" "info"
	completion_wrapper "pkg-install"        "root" "opkg" "install"
	#completion_wrapper "pkg-mark-auto"      "root" "opkg"
	#completion_wrapper "pkg-mark-manual"    "root" "opkg"
	completion_wrapper "pkg-owns"           ""     "opkg" "search"
	completion_wrapper "pkg-rconflicts"     ""     "opkg" "whatconflicts"
	completion_wrapper "pkg-refresh"        "root" "opkg" "update"
	completion_wrapper "pkg-reinstall"      "root" "opkg" "install" "--force-reinstall"
	completion_wrapper "pkg-remove"         "root" "opkg" "remove"
	
	completion_wrapper "pkg-depends"     ""     "opkg" "depends"
	completion_wrapper "pkg-rdepends"    ""     "opkg" "whatdepends"
	completion_wrapper "pkg-rrecommends" ""     "opkg" "whatrecommends"
	completion_wrapper "pkg-rreplaces"   ""     "opkg" "whatreplaces"
	completion_wrapper "pkg-rsuggests"   ""     "opkg" "whatsuggests"
	completion_wrapper "pkg-rprovides"   ""     "opkg" "whatprovides"
	
	# Implementations based on information exposed by OPKG
	completion_wrapper "pkg-search"     "" "__opkg_search"
	completion_wrapper "pkg-update"     "" "__opkg_update"
	completion_wrapper "pkg-upgrade"    "" "__opkg_update"
	
elif [ "${PKG_BACKEND}" = "zypper" ];
then
	completion_include zypper
	
	completion_wrapper "pkg-autoremove"     "root" "zypper" "remove" "--clean-deps"
	#completion_wrapper "pkg-build-dep"      "root"
	completion_wrapper "pkg-clean"          "root" "zypper" "clean"
	completion_wrapper "pkg-download"       ""     "zypper" "install" "--download-only"
	#completion_wrapper "pkg-forget-new"     "root" "zypper" "forget-new"
	completion_wrapper "pkg-hold"           "root" "zypper" "addlock"
	completion_wrapper "pkg-unhold"         "root" "zypper" "removelock"
	completion_wrapper "pkg-info"           ""     "zypper" "info"
	completion_wrapper "pkg-install"        "root" "zypper" "install"
	#completion_wrapper "pkg-mark-auto"      "root" "zypper" "markauto"
	#completion_wrapper "pkg-mark-manual"    "root" "zypper" "unmarkauto"
	#completion_wrapper "pkg-purge"          "root" "zypper" "purge"
	completion_wrapper "pkg-reinstall"      "root" "zypper" "install" "--force"
	completion_wrapper "pkg-remove"         "root" "zypper" "remove"
	completion_wrapper "pkg-search"         ""     "zypper" "--no-refresh" "search"
	completion_wrapper "pkg-source"         ""     "zypper" "source-download"
	completion_wrapper "pkg-versions"       ""     "zypper" "search" "--match-exact" "--details"
	
	completion_wrapper "pkg-refresh" "root" "zypper" "refresh"
	completion_wrapper "pkg-update"  "root" "zypper" "update"
	completion_wrapper "pkg-upgrade" "root" "zypper" "update"
	
	# rpm:
	completion_wrapper "pkg-changelog"    ""     "rpm" "--query" "--changelog"
	#completion_wrapper "pkg-configure"   "root"
	completion_wrapper "pkg-depends"      ""     "rpm" "--query" "--requires"
	completion_wrapper "pkg-files"        ""     "rpm" "--query" "--list"
	completion_wrapper "pkg-owns"         ""     "rpm" "--query" "--file"
	completion_wrapper "pkg-provides"     ""     "rpm" "--query" "--provides"
	completion_wrapper "pkg-rdepends"     ""     "rpm" "--query" "--whatrequires"
	
	
elif [ "${PKG_BACKEND}" = "aptitude" ];
then
	completion_include apt aptitude dpkg
	
	completion_wrapper "pkg-build-dep"      "root" "aptitude" "build-dep"
	completion_wrapper "pkg-changelog"      ""     "aptitude" "changelog"
	completion_wrapper "pkg-clean"          "root" "aptitude" "clean"
	completion_wrapper "pkg-download"       ""     "aptitude" "download"
	completion_wrapper "pkg-forget-new"     "root" "aptitude" "forget-new"
	completion_wrapper "pkg-hold"           "root" "aptitude" "hold"
	completion_wrapper "pkg-unhold"         "root" "aptitude" "unhold"
	completion_wrapper "pkg-info"           ""     "aptitude" "show"
	completion_wrapper "pkg-install"        "root" "aptitude" "install"
	completion_wrapper "pkg-mark-auto"      "root" "aptitude" "markauto"
	completion_wrapper "pkg-mark-manual"    "root" "aptitude" "unmarkauto"
	completion_wrapper "pkg-purge"          "root" "aptitude" "purge"
	completion_wrapper "pkg-refresh"        "root" "aptitude" "update"
	completion_wrapper "pkg-reinstall"      "root" "aptitude" "reinstall"
	completion_wrapper "pkg-remove"         "root" "aptitude" "remove"
	completion_wrapper "pkg-search"         ""     "aptitude" "search"
	completion_wrapper "pkg-source"         ""     "aptitude" "source"
	completion_wrapper "pkg-update"         "root" "aptitude" "safe-upgrade"
	completion_wrapper "pkg-upgrade"        "root" "aptitude" "full-upgrade"
	completion_wrapper "pkg-versions"       ""     "aptitude" "versions"
	
	# Aptitude does this automatically each time a packet is installed or removed:
	completion_wrapper "pkg-autoremove" "root" "apt" "autoremove"
	
	# apt:
	completion_wrapper "pkg-depends"      ""     "apt" "depends"
	completion_wrapper "pkg-rdepends"     ""     "apt" "rdepends"
	completion_wrapper "pkg-policy"       ""     "apt" "policy"
	completion_wrapper "pkg-showsrc"      ""     "apt" "showsrc"
	
	# dpkg:
	completion_wrapper "pkg-configure" "root" "dpkg" "--configure"
	completion_wrapper "pkg-files"     ""     "dpkg" "--listfiles"
	completion_wrapper "pkg-owns"      ""     "dpkg" "--search"
	
	__aptitude_rprovides()
	{
		aptitude search "?provides(${1})"
	}
	completion_wrapper "pkg-rprovides" "" "__aptitude_rprovides"
elif [ "${PKG_BACKEND}" = "apt-utils" ];
then
	completion_include apt aptitude dpkg
	
	# apt-get:
	completion_wrapper "pkg-autoremove"   "root" "apt" "autoremove"
	completion_wrapper "pkg-build-dep"    "root" "apt" "build-dep"
	completion_wrapper "pkg-changelog"    ""     "apt" "changelog"
	completion_wrapper "pkg-clean"        "root" "apt" "clean"
	completion_wrapper "pkg-depends"      ""     "apt" "depends"
	completion_wrapper "pkg-rdepends"     ""     "apt" "rdepends"
	completion_wrapper "pkg-download"     ""     "apt" "download"
	completion_wrapper "pkg-info"         ""     "apt" "show"
	completion_wrapper "pkg-install"      "root" "apt" "install"
	completion_wrapper "pkg-policy"       ""      "apt" "policy"
	completion_wrapper "pkg-purge"        "root" "apt" "purge"
	completion_wrapper "pkg-refresh"      "root" "apt" "update"
	completion_wrapper "pkg-reinstall"    "root" "apt" "--reinstall" "install"
	completion_wrapper "pkg-remove"       "root" "apt" "remove"
	completion_wrapper "pkg-search"       ""     "apt" "search"
	completion_wrapper "pkg-showsrc"      ""     "apt" "showsrc"
	completion_wrapper "pkg-source"       ""     "apt" "source"
	completion_wrapper "pkg-update"       "root" "apt" "upgrade"
	completion_wrapper "pkg-upgrade"      "root" "apt" "full-upgrade"
	
	# apt-mark (does not provide completion):
	completion_wrapper "pkg-hold"        "root" "apt-mark" "hold"
	completion_wrapper "pkg-unhold"      "root" "apt-mark" "unhold"
	completion_wrapper "pkg-mark-auto"   "root" "apt-mark" "auto"
	completion_wrapper "pkg-mark-manual" "root" "apt-mark" "manual"
	
	# dpkg:
	completion_wrapper "pkg-configure" "root" "dpkg" "--configure"
	completion_wrapper "pkg-files"     ""     "dpkg" "--listfiles"
	completion_wrapper "pkg-owns"      ""     "dpkg" "--search"
	completion_wrapper "pkg-versions"  ""     "dpkg" "--print-avail"
fi

builtin unset PKG_COMPLETE
builtin unset PKG_BACKEND
builtin unset -f completion_include
builtin unset -f completion_wrapper
